 <!-- ============ Pricing  ============= -->
<script type="text/javascript" src="js/pop_up.js"> </script>

        <section id ="pricing" class="description_content">
             <div class="pricing background_content">
                <h1><span>Los Mejores</span> restaurantes</h1>
             </div>
            <div class="text-content container"> 
                <div class="container">
                    <div class="row">
                        <div id="w">
                            <ul id="filter-list" class="clearfix">
                                <li class="filter" data-filter="todos">Todos</li>
                                <li class="filter" data-filter="primera">Primera</li><!--breakfast-->
                                <li class="filter" data-filter="segunda">Segunda</li><!--special-->
                                <li class="filter" data-filter="tercera">Tercera</li><!--desert-->
                                <li class="filter" data-filter="cuarta">Cuarta</li><!--dinner-->
                            </ul><!-- @end #filter-list -->    
                            <ul id="portfolio">
                                <li class="item primera">
                                <a href="internas/interna_dejavu.php"><img src="images/DEJA_VU.jpg" alt="Food" ></a>
                                </li>
                                <li class="item cuarta">
                                    <a href="#"><img src="images/mero.jpg" alt="Food" ></a>
                                </li>
                                <li class="item segunda">
                                    <a href=""><img src="images/200_millas.jpg" alt="Food"'></a>
                                </li>
                                <li class="item segunda">
                                    <a href=""><img src="images/fogon.jpg" alt="Food" ></a>
                                </li>
                                <li class="item segunda">
                                    <a href=""><img src="images/uruguayo.jpg" alt="Food" ></a>
                                </li>
                                <li class="item segunda">
                                    <a href=""><img src="images/sandi.jpg" alt="Food" ></a>
                                </li>
                                <li class="item segunda">
                                    <a href=""><img src="images/RICKY.jpg" alt="Food" ></a>
                                </li>
                                <li class="item segunda">
                                    <a href=""><img src="images/ch_farina.jpg" alt="Food" ></a>
                                </li>
                                <li class="item segunda">
                                    <a href="internas/interna_mamalola.php"><img src="images/mama_lola.jpg" alt="Food" ></a>
                                </li>
                                <li class="item tercera">
                                    <a href=""><img src="images/laBrasa.jpg" alt="Food" ></a>
                                </li>
                                <li class="item tercera">    
                                    <a href=""><img src="images/faisan.jpeg" alt="Food" ></a>
                                </li>
                                <li class="item tercera">    
                                    <a href=""><img src="images/pavi_pollo.jpg" alt="Food" ></a>
                                </li>
                                <li class="item tercera">    
                                    <a href=""><img src="images/perla.jpg" alt="Food" ></a>
                                </li>
                                <li class="item tercera">    
                                    <a href=""><img src="images/elrecreo.jpg" alt="Food" ></a>
                                </li>
                                <li class="item tercera">    
                                    <a href=""><img src="images/risotto.jpg" alt="Food" ></a>
                                </li>
                                <li class="item tercera">    
                                    <a href=""><img src="images/carbonero.jpg" alt="Food" ></a>
                                </li>
                                <li class="item tercera">    
                                    <a href=""><img src="images/beijing.png" alt="Food" ></a>
                                </li>
                                <!--<li class="item tercera primera"><img src="images/food_icon08.jpg" alt="Food" >
                                    <h2 class="white">$38</h2>
                                </li>-->
                            </ul><!-- @end #portfolio -->
                        </div><!-- @end #w -->
                    </div>
                </div>
            </div>  
        </section>