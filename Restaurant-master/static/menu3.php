<!--administrador general-->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="desconectar.php">Restaurantes Loja</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav main-nav  clear navbar-right ">
                            <li><a class="navactive color_animation" href="admin.php">INICIO</a></li>
                            <li><a class="color_animation" href="admin_restaurantes.php">RESTAURANTES</a></li>
                            <li><a class="color_animation" href="admin_bienes.php">BIENES</a></li>
                            <li><a class="color_animation" href="admin_catego.php">CATEGORÍAS</a></li>
                            <li><a class="color_animation" href="desconectar.php">SALIR</a></li>
                            <li><a class="color_animation" href="#"><strong><?php echo strtoupper($_SESSION['user']);?></a></strong></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </div><!-- /.container-fluid -->
        </nav>