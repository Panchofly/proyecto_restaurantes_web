<!DOCTYPE html>
<?php
session_start();
?>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/style-portfolio.css">
        <link rel="stylesheet" href="../css/picto-foundry-food.css" />
        <link rel="stylesheet" href="../css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>

    <?php
        extract($_GET);
        require("../static/connect_db.php");
        $hola2=$_SESSION['user'];
        //$sql="SELECT * FROM registro WHERE id_registro=$id";
        $sqll1="SELECT id_registro FROM registro WHERE user='$hola2'";
        $ressqll1=mysqli_query($mysqli,$sqll1);
        while ($row=mysqli_fetch_row ($ressqll1)){
            $id_reg=$row[0];
        }
        $sqll2="SELECT nombre_propietario FROM propietario WHERE id_registro='$id_reg'";
        $ressqll2=mysqli_query($mysqli,$sqll2);
        while ($row=mysqli_fetch_row ($ressqll2)){
            $nom_prop=$row[0];
        }
        $sqll3=("SELECT r.id_restaurante,p.nombre_propietario,r.nombre,r.representante,r.direccion,c.categoria, b.cantidad_mesas, re.user
               FROM restaurantes r, propietario p, registro re,categorias c,bienes b
               WHERE p.nombre_propietario = '$nom_prop' and p.id_ruc=r.id_ruc and p.id_registro=re.id_registro and r.id_categoria= c.id_categoria and p.id_ruc = b.id_ruc");
        $ressql2=mysqli_query($mysqli,$sqll3);
        //echo $ressql3;
        while ($row=mysqli_fetch_row ($ressql2)){
            $id_ret=$row[0];
            $nombre_propietario=$row[1];
            $nombre_rest=$row[2];
            $represen=$row[3];
            $direccion=$row[4];
            $categoria=$row[5];
            $cant_mesas=$row[6];
            $user=$row[7];
        }
    ?>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../index.php">Restaurantes Loja</a>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <section  id="reservation"  class="description_content">
            <div class="text-content container"> 
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="post" action="ejecutar_actualizar.php">
                            <!-- Left Inputs -->
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 col-md-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <!-- Name --> 
                                                <label for="text">Id Restaurante</label>                                               
                                                <input type="text" name="id_ret" class="form" value= "<?php echo $id_ret;?>" readonly="readonly">
                                                <label for="text">Nombre de Propietario</label>
                                                <input type="text" name="nom_prop" class="form" value="<?php echo $nombre_propietario;?>">
                                                <label for="text">Nombre de Restaurante</label>
                                                <input type="text" name="nomrest" class="form"  value="<?php echo $nombre_rest;?>">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <label for="text">Nombre de Representante</label>
                                                <input type="text" name="repre" class="form" value="<?php echo $represen;?>">
                                                <label for="text">Direccion</label>
                                                <input type="text" name="direccion" class="form"  value="<?php echo $direccion;?>">
                                                <label for="text">Categoria</label>
                                                <input type="text" name="catego" class="form"  value="<?php echo $categoria?>">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-xs-4">
                                                <label for="text">Cantidad de mesas</label>
                                                <input type="text" name="canti_mesas" class="form"  value="<?php echo $cant_mesas?>">
                                                <label for="text">Usuario</label>
                                                <input type="text" name="usera" class="form"  value="<?php echo $user;?>" readonly="readonly">
                                                <!-- Send Button -->
                                                <button type="submit" id="submit" value="3" name="submit" class="text-center form-btn form-btn">GUARDAR</button> 
                                                <!--<button type="submit" id="submit" name="submit" class="text-center form-btn form-btn">INGRESE</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>
        <?php 
            include("../static/footer.php");
        ?>


        <script type="text/javascript" src="../js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="../js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="../js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="../js/main.js" ></script>
    </body>
</html>
