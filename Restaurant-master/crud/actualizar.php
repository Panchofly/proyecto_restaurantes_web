<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/style-portfolio.css">
        <link rel="stylesheet" href="../css/picto-foundry-food.css" />
        <link rel="stylesheet" href="../css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>


    <?php
        extract($_GET);
        require("../static/connect_db.php");
        $sql="SELECT * FROM registro WHERE id_registro=$id";
        //la variable  $mysqli viene de connect_db que lo traigo con el require("connect_db.php");
        echo $id;
        $ressql=mysqli_query($mysqli,$sql);
        while ($row=mysqli_fetch_row ($ressql)){
            $id2=$row[0];
            $user=$row[1];
            $pass=$row[2];
            $email=$row[3];
            $pasadmin=$row[4];
        }
    ?>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="../index.php">Restaurantes Loja</a>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </nav>
        <section  id="reservation"  class="description_content">
            <div class="text-content container"> 
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="post" action="ejecutar_actualizar.php">
                            <!-- Left Inputs -->
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-md-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <!-- Name -->
                                                
                                                <input type="text" name="id" class="form" value= "<?php echo $id2?>" readonly="readonly">
                                                <input type="text" name="user" class="form" value="<?php echo $user?>">
                                                <input type="password" class="form" name="pass" value="<?php echo $pass?>">
                                                <input type="text" name="email" class="form" value="<?php echo $email?>">
                                                <input type="password" class="form" name="pasadmin" value="<?php echo $pasadmin?>" readonly="readonly">
                                            </div>
                                            <div class="col-xs-6 ">
                                                <!-- Send Button -->
                                                <button type="submit" id="submit" value="1" name="submit" class="text-center form-btn form-btn">GUARDAR</button> 
                                                <!--<button type="submit" id="submit" name="submit" class="text-center form-btn form-btn">INGRESE</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>
        <?php 
            include("../static/footer.php");
        ?>


        <script type="text/javascript" src="../js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="../js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="../js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="../js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="../js/main.js" ></script>
    </body>
</html>
