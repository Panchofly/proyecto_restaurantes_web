<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Restaurante</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style-portfolio.css">
        <link rel="stylesheet" href="css/picto-foundry-food.css" />
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/stilos.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>

    <body>

        <?php 
            include("static/menu.php");
         ?>
         
        <div id="top" class="starter_container bg">
            <div class="follow_container">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="top-title"> Restaurantes</h2>
                    <h2 class="white second-title">"Los mejores sitios de comida en Loja"</h2>
                    <hr>
                </div>
            </div>
        </div>

        <!-- ============ Acerca de ============= -->

        <section id="story" class="description_content">
            <div class="text-content container">
                <div class="col-md-6">
                    <h1>Acerca de</h1>
                    <div class="fa fa-cutlery fa-2x"></div>
                    <p class="desc-text">
                        Los restaurantes son lugares de simplicidad. Buena comida, buena cerveza y un buen servicio. Somos un pequeño grupo de Loja, Ecuador, que brinda a los restaurantes en Loja apertura a la publicidad y que da la posibilidd de que puedan ser conocidos. Ven y únete a nosotros y verás como te conoceran enseguida.
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="img-section">
                       <img src="images/kabob.jpg" width="250" height="220">
                       <img src="images/limes.jpg" width="250" height="220">
                       <div class="img-section-space"></div>
                       <img src="images/radish.jpg"  width="250" height="220">
                       <img src="images/corn.jpg"  width="250" height="220">
                   </div>
                </div>
            </div>
        </section>

        <!-- ============ Restaurantes  ============= -->

      <?php 
        include("static/presentacion.php");
       ?>


        <!-- ============ Nuestro Login ============= -->


        <section  id="login"  class="description_content">
            <div class="featured background_content">
                <h1>Ingrese con <span> su usuario</span></h1>
            </div>
            <div class="text-content container"> 
                <div class="inner contact">
                    <!-- Form Area -->
                    <div class="contact-form">
                        <!-- Form -->
                        <form id="contact-us" method="post" action="crud/validar.php">
                            <!-- Left Inputs -->
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-md-6 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-xs-6">
                                                <!-- Name -->
                                                <input type="text" name="realname" class="form" placeholder="Ingresa tu usuario" />
                                                <input type="password" name="pass" id="rpass" required="required" class="form" required placeholder="Ingresa password" />
                                            </div>
                                            <div class="col-xs-6 ">
                                                <!-- Send Button -->
                                                <button type="submit" id="submit" name="submit" class="text-center form-btn form-btn">INGRESE</button> 
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Clear -->
                            <div class="clear"></div>
                        </form>
                    </div><!-- End Contact Form Area -->
                </div><!-- End Inner -->
            </div>
        </section>


       <!-- ============ Nuestros Panes  ============= -->


        <section id="bread" class=" description_content">
            <div  class="bread background_content">
                <h1>Nuestros <span> Desayunos</span></h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-6">
                    <h1>NUESTROS DESAYUNOS</h1>
                    <div class="icon-bread fa-2x"></div>
                    <p class="desc-text">
                        Nos encanta el olor del pan recién horneado. Cada barra de pan está hecho a mano, utilizando sólo el más simple de los ingredientes para llevar a cabo los olores y sabores que invitan a todo el bloque. Detente y experimenta en cualquier momento la simplicidad en su máxima expresión.
                    </p>
                </div>
                <div class="col-md-6">
                    <img src="images/bread1.jpg" width="260" alt="Bread">
                    <img src="images/bread1.jpg" width="260" alt="Bread">
                </div>
            </div>
        </section>


        
        <!-- ============ Platos Ofrecidos  ============= -->

        <section id="featured" class="description_content">
            <div  class="featured background_content">
                <h1>Nuestro <span>Platos destacados</span></h1>
            </div>
            <div class="text-content container"> 
                <div class="col-md-6">
                    <h1>¡Nuestros Platos Lojanos!</h1>
                    <div class="icon-hotdog fa-2x"></div>
                    <p class="desc-text">
                        Cada comida es hecha a mano al amanecer, usando sólo el más simple de los ingredientes para sacar olores y sabores que invitan a todo el bloque. Deténgase en cualquier momento y experimente la simplicidad en su máxima expresión.
                    </p>
                </div>
                <div class="col-md-6">
                    <ul class="image_box_story2">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="images/slider1.jpg"  alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="images/slider2.jpg" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="images/slider3.JPG" alt="...">
                                    <div class="carousel-caption">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
        </section>

        <!-- ============ Registro Propietario  ============= -->
        <?php
            include_once('crud/menu_ingrese_prop.php')
        ?>
        <!-- ============ Seccion Social  ============= -->
      
        <section class="social_connect">
            <div class="text-content container"> 
                <div class="col-md-6">
                    <span class="social_heading">SIGUENOS</span>
                    <ul class="social_icons">
                        <li><a class="icon-twitter color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-github color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-linkedin color_animation" href="#" target="_blank"></a></li>
                        <li><a class="icon-mail color_animation" href="#"></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <span class="social_heading">O CONTACTANOS</span>
                    <span class="social_info"><a class="color_animation" href="tel:883-335-6524">(941) 883-335-6524</a></span>
                </div>
            </div>
        </section>

        <!-- ============ Registro Usuarios  ============= -->

        <?php include_once('crud/menu_ingrese.php') ?>

        <!-- ============ Sección Footer  ============= -->

        <?php   include_once('static/footer.php') ?>


        <script type="text/javascript" src="js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="js/main.js" ></script>

    </body>
</html>