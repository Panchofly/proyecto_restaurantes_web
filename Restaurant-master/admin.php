<!DOCTYPE html>
<?php
session_start();
if (@!$_SESSION['user']) {
    header("Location:index.php");
}elseif ($_SESSION['rol']==2) {
    header("Location:index2.php");
}
?>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Restaurant</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style-portfolio.css">
        <link rel="stylesheet" href="css/picto-foundry-food.css" />
        <link rel="stylesheet" href="css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>

    <body>

        <?php 
            include("static/menu3.php");
        ?>

        <section  id="reservation"  class="description_content">
            <div class="well well-small">
            <hr class="soft"/>
            <div class="jumbotron">
                <h2>ADMINISTRADOR DE USUARIOS REGISTRADOS</h2>     
            </div>
            
            <div class="row-fluid">
                <?php

                    require("static/connect_db.php");
                    $sql=("SELECT * FROM registro");
        
    //la variable  $mysqli viene de connect_db que lo traigo con el require("connect_db.php");
                    $query=mysqli_query($mysqli,$sql);

                    echo "<table border='1'; class='table table-hover';>";
                        echo "<tr class='warning'>";
                            echo "<td>Id</td>";
                            echo "<td>Usaurio</td>";
                            echo "<td>Password</td>";
                            echo "<td>Correo</td>";
                            echo "<td>Password del administrador</td>";
                            echo "<td>Rol</td>";
                            echo "<td>Editar</td>";
                            echo "<td>Borrar</td>";
                        echo "</tr>";

                    
                ?>
                  
                <?php 
                     while($arreglo=mysqli_fetch_array($query)){
                        echo "<tr class='success'>";
                            echo "<td>$arreglo[0]</td>";
                            echo "<td>$arreglo[1]</td>";
                            echo "<td>$arreglo[2]</td>";
                            echo "<td>$arreglo[3]</td>";
                            echo "<td>$arreglo[4]</td>";
                            echo "<td>$arreglo[5]</td>";

                            echo "<td><a href='crud/actualizar.php?id=$arreglo[0]'><img src='images/actualizar.gif' class='img-rounded'></td>";
                            echo "<td><a href='admin.php?id=$arreglo[0]&idborrar=$arreglo[5]'><img src='images/eliminar.png' class='img-rounded'/></a></td>";    
                        echo "</tr>";
                    }

                    echo "</table>";

                        extract($_GET);
                        if(@$idborrar==3 || @$idborrar==2){
            
                            $sqlborrar="DELETE FROM registro WHERE id_registro=$id";
                            $resborrar=mysqli_query($mysqli,$sqlborrar);
                            echo '<script>alert("REGISTRO ELIMINADO")</script> ';
                            //header('Location: proyectos.php');
                            echo "<script>location.href='admin.php'</script>";
                        }

                ?>  
            </div>  
        </section>
       
        <?php 
            include("static/footer.php");
        ?>


        <script type="text/javascript" src="js/jquery-1.10.2.min.js"> </script>
        <script type="text/javascript" src="js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="js/jquery-1.10.2.js"></script>     
        <script type="text/javascript" src="js/jquery.mixitup.min.js" ></script>
        <script type="text/javascript" src="js/main.js" ></script>

    </body>
</html>
