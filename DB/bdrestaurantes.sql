-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-05-2017 a las 01:01:21
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdrestaurantes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bienes`
--

CREATE TABLE `bienes` (
  `id_bienes` int(30) NOT NULL,
  `cantidad_mesas` varchar(30) NOT NULL,
  `id_ruc` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bienes`
--

INSERT INTO `bienes` (`id_bienes`, `cantidad_mesas`, `id_ruc`) VALUES
(1, '16', '1101022505001'),
(2, '20', '1100068897001'),
(3, '9', '1100569753001'),
(4, '29', '1100060897001    '),
(5, '9', '1102654793001'),
(6, '10', '1101608440001'),
(7, '9', '1102551247001'),
(8, '9', '1100546470001'),
(9, '22', '1101713194001'),
(10, '8', '1102056056001'),
(11, '14', '1101853594001'),
(12, '44', '1102013727001'),
(13, '12', '1104240914001'),
(14, '10', '1101744322001'),
(15, '9', '1790527085001'),
(16, '17', '1103754659001'),
(17, '9', '1102930896001'),
(18, '10', '1102061817001'),
(19, '15', '1104449093001'),
(20, '14', '1103669345001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bienes_restaurantes`
--

CREATE TABLE `bienes_restaurantes` (
  `id_bi_res` int(11) NOT NULL,
  `id_bienes` int(30) NOT NULL,
  `id_restaurante` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bienes_restaurantes`
--

INSERT INTO `bienes_restaurantes` (`id_bi_res`, `id_bienes`, `id_restaurante`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 9, 9),
(10, 10, 10),
(11, 11, 11),
(12, 12, 12),
(13, 13, 13),
(14, 14, 14),
(15, 15, 15),
(16, 16, 16),
(17, 17, 17),
(18, 18, 18),
(19, 19, 19),
(20, 20, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(30) NOT NULL,
  `categoria` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `categoria`) VALUES
(1, 'TERCERA'),
(2, 'TERCERA'),
(3, 'TERCERA'),
(4, 'TERCERA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `propietario`
--

CREATE TABLE `propietario` (
  `id_ruc` varchar(40) NOT NULL,
  `cedula` varchar(40) NOT NULL,
  `nombre_propietario` varchar(100) NOT NULL,
  `telefono` int(30) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `id_registro` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `propietario`
--

INSERT INTO `propietario` (`id_ruc`, `cedula`, `nombre_propietario`, `telefono`, `correo`, `id_registro`) VALUES
('1100060897001    ', '1101500160', 'FELICIANO ISRAEL AZANZA ORDONIEZ', 2584474, '', 5),
('1100068897001    ', '1101500155', 'FELICIANO ISRRAEL AZANZA ORDONIEZ', 2584474, '', 3),
('1100546470001    ', '1101500182', 'ALBA VIOLETA BRAVO CHUQUIMARCA', 2571455, '', 9),
('1100569753001    ', '1101500156', 'IRMA ASTREJILDA CASTRO AGUIRRE', 2581813, '', 4),
('1101022505001    ', '1101500148', 'VISMAR AUGUSTO MONCAYO VACA', 2573563, '', 2),
('1101608440001    ', '1101500170', 'JOSE DAVID JIMA JIMA', 2571381, '', 7),
('1101713194001    ', '1101500193', 'BYRON FERNANDO RIOFRIO CORONEL', 2576150, 'sinan111@hotmail.com', 10),
('1101744322001    ', '1101500388', 'POMA LOJA MARIETHA ISABEL', 0, '', 15),
('1101853594001    ', '1101500208', 'PIEDAD ROSARIO NARANJO CABRERA', 2579248, '', 12),
('1102013727001    ', '1101500211', 'ENRIQUE ANTONIO TENORIO ORTIZ', 2581264, '', 13),
('1102056056001    ', '1101500196', 'GRACIELA ELVIRA AVILES RIERA', 2578924, '', 11),
('1102061817001    ', '1101500849', 'LOJAN GALLARDO DORASTENIA', 2583984, '', 19),
('1102551247001    ', '1101500181', 'DARWIN SEGUNDO CHUQUIMARCA B', 2587964, 'cbmax6@gmail.com', 8),
('1102654793001    ', '1101500161', 'LEOVANI PATRICIO RIOS HIDALGO', 2587800, '', 6),
('1102930896001    ', '1101500686', 'TORRES CASTRO EMILIANO ANTONIO', 93905437, '', 18),
('1103152268001', '1101501167', 'DÁVILA VARGAS ALEXANDRA ELIZABETH', 2582347, 'aedavila@gmail.com', 22),
('1103669345001    ', '1101501035', 'AZANZA ORTIZ MARITZA ELIZABETH', 257494969, '', 21),
('1103754659001    ', '1101500605', 'ESPINOSA VERA CRISTIAN FERNANDO', 2560216, '', 17),
('1104240914001    ', '1101500221', 'MOROCHO VILLA MIRIAN VANESSA', 0, '', 14),
('1104449093001    ', '1101500996', 'BENTACOURT VALDIVIESO CARLOS ANDRES', 2584790, 'chifa_beijing@outlook.es', 20),
('1790527085001    ', '1101500553', 'CITYMAXIS S.A.', 2581819, 'loja@chefarina.com', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro`
--

CREATE TABLE `registro` (
  `id_registro` int(15) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pasadmin` int(10) NOT NULL,
  `rol` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `registro`
--

INSERT INTO `registro` (`id_registro`, `user`, `password`, `email`, `pasadmin`, `rol`) VALUES
(1, 'administrador', '', 'admin@utpl', 1234, '1'),
(2, 'vamoncayo', '1234', 'vamoncayo@gmail.com', 0, '3'),
(3, 'fiazanza', '1234', 'fiazanza@hotmail.es', 0, '3'),
(4, 'iacastro', '1234', 'iacastro@yahoo.com', 0, '3'),
(5, 'fiazanza1', '1234', 'fiazanza1@gmail.com', 0, '3'),
(6, 'larios', '1234', 'larios@gmail.com', 0, '3'),
(7, 'jdjima', '1234', 'jdjima@hotmail.com', 0, '3'),
(8, 'dschuquimarca', '1234', 'cbmax6@gmail.com', 0, '3'),
(9, 'avbravo', '1234', 'avbravo@yahoo.com', 0, '3'),
(10, 'bfriofrio', '1234', 'sinan111@hotmail.com', 0, '3'),
(11, 'geaviles', '1234', 'geaviles@gmail.com', 0, '3'),
(12, 'prnaranjo', '1234', 'prnaranjo@net', 0, '3'),
(13, 'eatenorio', '1234', 'eatenorio@hotmail.com', 0, '3'),
(14, 'mvmirian', '1234', 'mvmirian@hotmail.com', 0, '3'),
(15, 'plmarietha', '1234', 'plmarietha@hotmail.com', 0, '3'),
(16, 'citymaxis', '1234', 'loja@chefarina.com', 0, '3'),
(17, 'evcristian', '1234', 'evcristian@hotmail.com', 0, '3'),
(18, 'tcemiliano', '1234', 'tcemiliano@hotmail.com', 0, '3'),
(19, 'lggallardo', '1234', 'lggallardo@hotmail.com', 0, '3'),
(20, 'bvcarlos', '1234', 'chifa_beijing@outlook.es', 0, '3'),
(21, 'aormaritza', '1234', 'aormaritza@hotmail.com', 0, '3'),
(22, 'aedavila', '1234', 'aedavila@gmail.com', 0, '3'),
(23, 'luzon', '12345', 'luzon@utpl.edu.ec', 0, '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

CREATE TABLE `reservas` (
  `id_reserva` int(11) NOT NULL,
  `nombre_usuario` varchar(20) NOT NULL,
  `estado` varchar(15) NOT NULL,
  `fecha` date NOT NULL,
  `telefono` int(11) NOT NULL,
  `numero_invitados` int(11) NOT NULL,
  `correo` varchar(200) NOT NULL,
  `tema` varchar(200) NOT NULL,
  `id_restaurante` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reservas`
--

INSERT INTO `reservas` (`id_reserva`, `nombre_usuario`, `estado`, `fecha`, `telefono`, `numero_invitados`, `correo`, `tema`, `id_restaurante`) VALUES
(1, 'Luzon.Fancisco', 'activo', '2017-05-30', 123456789, 3, 'luzon@utpl.edu.ec', 'romÃ¡ntico', 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurantes`
--

CREATE TABLE `restaurantes` (
  `id_restaurante` int(30) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `representante` varchar(100) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `id_ruc` varchar(40) NOT NULL,
  `id_categoria` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `restaurantes`
--

INSERT INTO `restaurantes` (`id_restaurante`, `nombre`, `representante`, `direccion`, `id_ruc`, `id_categoria`) VALUES
(1, '200 MILLAS', 'VISMAR AUGUSTO MONCAYO VACA', 'JUAN J PENA 07-41 Y 10 DE AGOSTO', '1101022505001    ', 2),
(2, 'PARRILLADAS EL FOGON', 'FELICIANO ISRRAEL AZANZA ORDONIEZ', 'AV 8 DE DICIEMBRE Y JUAN JOSE FLORES', '1100068897001    ', 2),
(3, 'PARRILLADAS URUGUAYAS', 'IRMA ASTREJILDA CASTRO AGUIRRE', 'JUAN DE SALINAS Y UNIVERSITARIA', '1100569753001    ', 2),
(4, 'SANDY', 'FELICIANO ISRAEL AZANZA ORDONIEZ', 'AV. 8 DE DICIEMBRE Y JUAN JOSE FLORES', '1100060897001    ', 2),
(5, 'SANDY CENTRO', 'LEOVANI PATRICIO RIOS HIDALGO', 'MERCADILLO Y BOLIVAR', '1102654793001    ', 2),
(6, 'LA BRASA ', 'JOSE DAVID JIMA JIMA', 'ROCAFUERTE 1317 Y AV IBEROAMERICA', '1101608440001    ', 3),
(7, 'FAISAN  EL SUC.', 'DARWIN SEGUNDO CHUQUIMARCA B', 'TERMINAL TERRESTRE', '1102551247001    ', 3),
(8, 'FAISAN PRINCIPAL', 'ALBA VIOLETA BRAVO CHUQUIMARCA', '10 DE AGOSTO Y LAURO GUERRERO', '1100546470001    ', 3),
(9, 'PAVI POLLO N 2', 'BYRON FERNANDO RIOFRIO CORONEL', 'MERCADILLO Y SUCRE ESQUINA.', '1101713194001    ', 3),
(10, 'PERLA DEL PACIFICO', 'GRACIELA ELVIRA AVILES RIERA', 'J.A EGUIGUREN  1625 Y 18 NOVIEMBRE', '1102056056001    ', 3),
(11, 'EL RECREO ', 'PIEDAD ROSARIO NARANJO CABRERA', 'SALVADOR B. CELI Y CHONE', '1101853594001    ', 3),
(12, 'RICKY', 'ENRIQUE ANTONIO TENORIO ORTIZ', 'MERCADILLO Y JOSE MARIA PENIA ESQ.', '1102013727001    ', 2),
(13, 'A LO MERO MERO', 'MOROCHO VILLA MIRIAN VANESA', 'SUCRE Y COLON', '1104240914001    ', 4),
(14, 'RIZZOTO', 'POMA LOJA MARIETHA ISABEL', 'MIGUEL RIOFRIO Y BOLIVAR', '1101744322001    ', 3),
(15, 'CH FARINA', 'CUEVA SONO CLEMENCIA ELIZABETH', 'AV. ORILLAS DEL ZAMORA Y GUAYAQUIL', '1790527085001    ', 2),
(16, 'CARBONERO SUC. EL', 'ESPINOSA VERA CRISTIAN FERNANDO', 'AV. 24 DE MAYO Y MIGUEL RIOFRIO', '1103754659001    ', 3),
(17, 'FOGON GRILL', 'TORRES CASTRO EMILIANO ANTONIO', 'TORRES CASTRO EMILIANO ANTONIO', '1102930896001    ', 2),
(18, 'MAMA LOLA', 'LOJAN GALLARDO DORASTENIA', 'AV, SALVADOR BUSTAMENTE CELI Y SANTA ROSA', '1102061817001    ', 2),
(19, 'BEIJING SUC.', 'BENTACOURT VALDIVIESO CARLOS ANDRES', 'AV. PIO JARAMILLO Y QUENEDY', '1104449093001    ', 3),
(20, 'FOGON GRILL CENTRO EL', 'AZANZA ORTIZ MARITZA ELIZABETH', 'OLMEDO Y JOSE ANTONIO EGUIGUREN', '1103669345001    ', 2),
(22, 'DEJA VU', 'DAVILA VARGAS ALEXANDRA ELIZABETH', 'BERNARDO VALDIVIESO E/ 10 DE AGOSTO Y JOSÉ A. EGUIGUREN', '1103152268001', 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bienes`
--
ALTER TABLE `bienes`
  ADD PRIMARY KEY (`id_bienes`),
  ADD KEY `fk_id_ruc_propietario` (`id_ruc`),
  ADD KEY `id_ruc` (`id_ruc`);

--
-- Indices de la tabla `bienes_restaurantes`
--
ALTER TABLE `bienes_restaurantes`
  ADD PRIMARY KEY (`id_bi_res`),
  ADD KEY `fk_id_bienes_bienes` (`id_bienes`),
  ADD KEY `fk_id_restaurante_restaurante` (`id_restaurante`),
  ADD KEY `id_bienes` (`id_bienes`),
  ADD KEY `id_restaurante` (`id_restaurante`),
  ADD KEY `id_bienes_2` (`id_bienes`),
  ADD KEY `id_restaurante_2` (`id_restaurante`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `propietario`
--
ALTER TABLE `propietario`
  ADD PRIMARY KEY (`id_ruc`),
  ADD KEY `fk_id_registro_registro` (`id_registro`),
  ADD KEY `id_registro` (`id_registro`),
  ADD KEY `id_registro_2` (`id_registro`);

--
-- Indices de la tabla `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`id_registro`);

--
-- Indices de la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD PRIMARY KEY (`id_reserva`),
  ADD KEY `id_restaurante` (`id_restaurante`);

--
-- Indices de la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  ADD PRIMARY KEY (`id_restaurante`),
  ADD KEY `fk_id_ruc_propietarios` (`id_ruc`),
  ADD KEY `fk_id_categoria_categoria` (`id_categoria`),
  ADD KEY `id_ruc` (`id_ruc`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `id_categoria_2` (`id_categoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bienes`
--
ALTER TABLE `bienes`
  MODIFY `id_bienes` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `bienes_restaurantes`
--
ALTER TABLE `bienes_restaurantes`
  MODIFY `id_bi_res` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `registro`
--
ALTER TABLE `registro`
  MODIFY `id_registro` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `reservas`
--
ALTER TABLE `reservas`
  MODIFY `id_reserva` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  MODIFY `id_restaurante` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bienes`
--
ALTER TABLE `bienes`
  ADD CONSTRAINT `fk_id_ruc2_propietario` FOREIGN KEY (`id_ruc`) REFERENCES `propietario` (`id_ruc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `bienes_restaurantes`
--
ALTER TABLE `bienes_restaurantes`
  ADD CONSTRAINT `fk_id_bienes_bienes` FOREIGN KEY (`id_bienes`) REFERENCES `bienes` (`id_bienes`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_restaurante_restaurante` FOREIGN KEY (`id_restaurante`) REFERENCES `restaurantes` (`id_restaurante`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `propietario`
--
ALTER TABLE `propietario`
  ADD CONSTRAINT `fk_id_registro_registro` FOREIGN KEY (`id_registro`) REFERENCES `registro` (`id_registro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD CONSTRAINT `fk_id_restaurante_restaurantes` FOREIGN KEY (`id_restaurante`) REFERENCES `restaurantes` (`id_restaurante`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `restaurantes`
--
ALTER TABLE `restaurantes`
  ADD CONSTRAINT `fk_id_categoria_categorias` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_ruc_propietario` FOREIGN KEY (`id_ruc`) REFERENCES `propietario` (`id_ruc`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
